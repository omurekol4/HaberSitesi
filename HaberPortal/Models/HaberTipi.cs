namespace HaberPortal
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HaberTipi")]
    public partial class HaberTipi
    {
        public HaberTipi()
        {
            Haber = new HashSet<Haber>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Adi { get; set; }

        public virtual ICollection<Haber> Haber { get; set; }
    }
}
