namespace HaberPortal
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Haber")]
    public partial class Haber
    {
        public Haber()
        {
            Resim = new HashSet<Resim>();
            Yorum = new HashSet<Yorum>();
            Etiket = new HashSet<Etiket>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(70)]
        public string Baslik { get; set; }

        [Required]
        [StringLength(200)]
        public string Ozet { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string Icerik { get; set; }

        public DateTime YayimTarihi { get; set; }

        public Guid? YazarID { get; set; }

        public int KategoriID { get; set; }

        public int TipID { get; set; }

        [Required]
        [StringLength(200)]
        public string ResimYol { get; set; }
         
        [StringLength(250)]
        public string VideoYol { get; set; }

        [Required]
        [StringLength(200)]
        public string KucukResimYol { get; set; }

        public int Goruntulenme { get; set; }

        public int? YorumSayisi { get; set; }
       
         

        public virtual aspnet_Users aspnet_Users { get; set; }

        public virtual HaberTipi HaberTipi { get; set; }

        public virtual Kategori Kategori { get; set; }

        public virtual ICollection<Resim> Resim { get; set; }

        public virtual ICollection<Yorum> Yorum { get; set; }

        public virtual ICollection<Etiket> Etiket { get; set; }
    }
}
