namespace HaberPortal
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Etiket")]
    public partial class Etiket
    {
        public Etiket()
        {
            Haber = new HashSet<Haber>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Adi { get; set; }

        public virtual ICollection<Haber> Haber { get; set; }
    }
}
