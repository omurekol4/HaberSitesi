﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HaberPortal.Controllers
{
    public class HaberController : Controller
    {
        //
        // GET: /Haber/
        HaberModel db = new HaberModel();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Goster(int id)
        {
            return View(db.Haber.FirstOrDefault(x=>x.Id== id));
        }

        public ActionResult YorumYap(String txtAdSoyad,String txtMail,String txtIcerik,int ID) {
            Yorum yorum = new Yorum();
            yorum.AdSoyad = txtAdSoyad;
            yorum.Icerik = txtIcerik;
            yorum.Mail = txtMail;
            yorum.HaberId = ID;
            yorum.Ip = Request.ServerVariables["REMOTE_ADDR"];
            yorum.Baslik = "Yorum";
            yorum.Tarih = DateTime.Now;
            yorum.Begen = 0;
            yorum.Tiksinti = 0;
            yorum.Onayli = true;
            db.Yorum.Add(yorum);
            db.SaveChanges();
            return RedirectToAction("Goster", new { id= ID } );
        }

        public int YorumBegen(int id ) {
            Yorum yrm = db.Yorum.FirstOrDefault(x => x.Id == id);
            yrm.Begen++;
            db.SaveChanges();
            return yrm.Begen - yrm.Tiksinti;

        }
        public int YorumTiksin(int id)
        {
            Yorum yrm = db.Yorum.FirstOrDefault(x => x.Id == id);
            yrm.Tiksinti++;
            db.SaveChanges();
            return yrm.Begen - yrm.Tiksinti;
        }

    }
}
