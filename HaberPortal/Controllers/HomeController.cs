﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HaberPortal.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/ 
        HaberModel db = new HaberModel();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SliderGetir(){
            var haberler = db.Haber.Where(x=>x.TipID==1).OrderByDescending(x=>x.YayimTarihi).Take(10);
            return View(haberler);
        }
        public ActionResult EnSonHaberler() {
            var haberler = db.Haber.Where(x=>x.TipID==3).OrderByDescending(x=>x.YayimTarihi).Take(10);
            return View(haberler);
        }
        public ActionResult Videolar() {
            var videolar = db.Haber.Where(x => x.VideoYol != null).OrderByDescending(x => x.YayimTarihi);
            var rastgeleVideo = RandomIcerik(videolar,4);

            return View(rastgeleVideo);
        }
        public ActionResult HaberDetayi() {
            return View();
        }
        public ActionResult RastgeleHaberler() {

            var  haberler = db.Haber.Where(x=>x.TipID!=2).OrderByDescending(x => x.YayimTarihi);
           
             
            var hbr = RandomIcerik(haberler, 5);
            

            return View(hbr);
        }
        /// <summary>
        /// Random içerik getirir.
        /// </summary>
        /// <param name="rastgeleHaber"> içerik listesi </param>
        /// <param name="num"> içerik sayısı </param>
        /// <returns></returns>
        public Haber[] RandomIcerik(IEnumerable<HaberPortal.Haber> rastgeleHaber,int num  ) {
            int index;
            int sayac = 0;
            int count = rastgeleHaber.Count();
            int[] idList = new int[count];
            foreach (var haber in rastgeleHaber)
            { 
                idList[sayac] = haber.Id;
                sayac++;
            }
            int maxValue = idList.Max();
            int minValue = idList.Min();
            Haber[] hbr = new Haber[num];
            for (int i = 0; i < num; i++)
            { 
                index = new Random().Next(minValue, maxValue);
                hbr[i] = rastgeleHaber.Where(x => x.Id == index).FirstOrDefault();
                if (hbr[i] == null)
                { 
                    while (hbr[i] == null) {
                        index = new Random().Next(minValue, maxValue);
                        hbr[i] = rastgeleHaber.Where(x => x.Id == index).FirstOrDefault();
                    }
                }
            }
            for (int j = num-1; j >= 0; j--)
            {
                int rec = 0;//tekrar sayısını tutar.
                foreach (var b in hbr)
                {
                    if (b == hbr[j])
                    {//en fazla 1 kez aynı olabilir.2 kez aynıysa tekrar random üret.
                        rec++;
                        if (rec > 1)//ikinci tekrarı önler
                        {
                            index = new Random().Next(count);
                            hbr[j] = rastgeleHaber.Skip(index).FirstOrDefault();
                        }
                    }
                }
            }

            return hbr;
        }

    }
}
